#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import ConfigParser
from plexapi.myplex import MyPlexAccount
from flask_socketio import SocketIO, emit
from flask import Flask, render_template, request, redirect, flash
import utils
import Queue
import threading
import os
import requests
import sys
import eventlet
import logging

eventlet.monkey_patch()

# if utils.module_exists("pync"):
#    from pync import Notifier

config = ConfigParser.ConfigParser()
print(os.path.dirname(sys.executable))

if getattr(sys, 'frozen', False):
    config.read(os.path.join(os.path.dirname(sys.executable), "config.ini"))
else:
    config.read(os.path.join(os.path.dirname(sys.argv[0]), "config.ini"))

path = config.get("Main", "SavePath")

if path == "" or not os.path.isdir(path):
    print("SavePath not set or not exists")
    exit(1)

app = Flask(__name__)
app.config.update({
    'TEMPLATES_AUTO_RELOAD': True
})

sio = SocketIO(app)
sys.stdout.write("Logging in Plex... ")
sys.stdout.flush()
plex_account = MyPlexAccount.signin(config.get("Main", "PlexUsername"), config.get("Main", "PlexPassword"))
print("Done")
sys.stdout.write("Connecting to selected server... ")
sys.stdout.flush()
print(plex_account.resources())
plex_server = plex_account.resource(config.get("Main", "PlexServer").strip()).connect()
print("Done")
sys.stdout.write("Querying sections... ")
sys.stdout.flush()
plex_sections = plex_server.library.sections()
print("Done")
queue = Queue.Queue()
queue_list = []

qualities = [
    {
        'title': '720x480 (400 MB per 60M)',
        'bitrate': '720',
        'resolution': '720x480'
    }
]

class ThreadDownload(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            data = self.queue.get()
            media = data['media']
            section = data['section']
            url = media.getStreamURL(maxVideoBitrate=config.get('Transcode', 'MaxBitrate'),
                                                 videoResolution=config.get('Transcode', 'VideoResolution'))
            name = ""
            final_path = ""

            if not os.path.isdir(os.path.join(path, section.title)):
                os.makedirs(os.path.join(path, section.title))

            if media.__class__.__name__ == "Movie":
                name = media.title
                final_path = os.path.join(path, section.title)
            elif media.__class__.__name__ == "Episode":
                show = data['show']
                season_number = media.seasonNumber
                section_path = os.path.join(path, section.title)
                if not os.path.isdir(os.path.join(os.path.join(section_path, show.title), 'Season ' + str(season_number).zfill(2))):
                    os.makedirs(os.path.join(os.path.join(section_path, show.title), 'Season ' + str(season_number).zfill(2)))
                final_path = os.path.join(os.path.join(section_path, show.title), 'Season ' + str(season_number).zfill(2))
                name = show.title + ' - ' + 'S' + str(media.seasonNumber).zfill(2) + 'E' + str(media.index).zfill(3) + ' - ' + media.title

            queue_item_index = utils.search_in_dictionary_array(queue_list, "rating_key", media.ratingKey)

            if not os.path.exists(os.path.join(final_path, name + '.mkv')):
                # if utils.module_exists("pync"):
                #    Notifier.notify(name + " started.", title="PlexSync")

                if queue_item_index != -1:
                    queue_list[queue_item_index]['status'] = 1

                print("Downloading " + name)
                print(url)
                r = requests.get(url, stream=True)
                total_size = media.duration/1000*int(config.get('Transcode', 'MaxBitrate'))/8*1024
                tmp_size = 0
                with open(os.path.join(final_path, name + '.mkv'), 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        if chunk:
                            tmp_size = tmp_size + len(chunk)
                            f.write(chunk)
                            f.flush()
                            if queue_item_index != -1:
                                if queue_list[queue_item_index]['percentage'] != utils.percentage(total_size, tmp_size):
                                    queue_list[queue_item_index]['percentage'] = utils.percentage(total_size, tmp_size)
                                    sio.emit("queue_update", {"id": queue_list[queue_item_index]['rating_key'], "percentage": utils.percentage(total_size, tmp_size)})
                                    sys.stdout.write("\r[%s%s]" % ('=' * (utils.percentage(total_size, tmp_size)/2), ' ' * (50 - utils.percentage(total_size, tmp_size)/2)))
                                    sys.stdout.flush()

                print("\nFinished " + name)
                sio.emit("queue_remove", {"id": queue_list[queue_item_index]['rating_key']})
                sio.emit("notification", {"body": name + " downloaded."})
            if queue_item_index != -1:
                del queue_list[queue_item_index]
            # if utils.module_exists("pync"):
            #    Notifier.notify(name + " downloaded.", title="PlexSync")
            self.queue.task_done()


@app.route('/')
def index():
    return render_template('sections.html')

@app.route('/section/<section_id>')
@app.route('/section/<section_id>/<mode>')
def section(section_id, mode=None):
    plex_section = plex_server.library.sectionByID(section_id)
    if "MovieSection" == plex_section.__class__.__name__ or plex_section.__class__.__name__ == "ShowSection":
        if plex_section.__class__.__name__ == "MovieSection":
            movies = plex_section.all()
            movies_matrix = utils.array_split(movies, 6)
            if mode == "covers":
                return render_template('movies_covers.html', movies_matrix=movies_matrix, section_name=plex_section.title,
                                       section_id=plex_section.key, plex_server=plex_server)
            else:
                return render_template('movies.html', movies=movies, section_name=plex_section.title,
                                   section_id=plex_section.key)
        else:
            shows = plex_section.all()
            return render_template('shows.html', shows=shows, section_name=plex_section.title,
                                   section_id=plex_section.key)
    else:
        return render_template('not_implemented.html')


@app.route('/section/<section_id>/show/<show_title>/seasons')
def section_seasons(section_id, show_title):
    plex_section = plex_server.library.sectionByID(section_id)
    show = plex_section.get(show_title)
    seasons = show.seasons()
    return render_template('seasons.html', seasons=seasons, show=show, section_id=section_id, plex_server=plex_server)


@app.route('/section/<section_id>/show/<show_title>/season/<season_title>')
def section_episodes(section_id, show_title, season_title):
    plex_section = plex_server.library.sectionByID(section_id)
    show = plex_section.get(show_title)
    season = show.season(season_title)
    episodes = season.episodes()
    return render_template('episodes.html', episodes=episodes, season=season, show=show, section_id=section_id)


@app.route('/section/<section_id>/download_movie/<media_name>')
def section_film_download(section_id, media_name):
    plex_section = plex_server.library.sectionByID(section_id)
    movie = plex_section.get(media_name)
    queue_list.append({'title': movie.title, 'status': 0, 'rating_key': movie.ratingKey, 'percentage': 0})
    queue.put({'media': movie,
               'section': plex_section,
               })
    flash(movie.title + u' added to queue.', category='success')
    return redirect('/section/' + section_id)


@app.route('/section/<section_id>/download_show/<show_name>')
@app.route('/section/<section_id>/download_show/<show_name>/<season_name>')
@app.route('/section/<section_id>/download_show/<show_name>/<season_name>/<episode_name>')
def section_show_download(section_id, show_name, season_name=None, episode_name=None):
    plex_section = plex_server.library.sectionByID(section_id)
    show = plex_section.get(show_name)
    finish_message = ''
    if season_name == None and episode_name == None:
        episodes = show.episodes()
        for episode in episodes:
            episode_title = show.title + ' - ' + 'S' + str(episode.seasonNumber).zfill(2) + 'E' + str(episode.index).zfill(3) + ' - ' + episode.title
            queue_list.append({'title': episode_title, 'status': 0, 'rating_key': episode.ratingKey, 'percentage': 0})
            queue.put({'media': episode,
                       'section': plex_section,
                       'show': show
                       })
        finish_message = show.title + ' added to downloads'
    elif season_name != None and episode_name == None:
        season = show.season(season_name)
        episodes = season.episodes()
        for episode in episodes:
            episode_title = show.title + ' - ' + 'S' + str(episode.seasonNumber).zfill(2) + 'E' + str(
                episode.index).zfill(3) + ' - ' + episode.title
            queue_list.append({'title': episode_title, 'status': 0, 'rating_key': episode.ratingKey, 'percentage': 0})
            queue.put({'media': episode,
                       'section': plex_section,
                       'show': show
                       })
        finish_message = season.title + ' of ' + show.title + ' added to downloads'
    else:
        season = show.season(season_name)
        episode = season.episode(episode_name)
        episode_title = show.title + ' - ' + 'S' + str(episode.seasonNumber).zfill(2) + 'E' + str(
            episode.index).zfill(3) + ' - ' + episode.title
        queue_list.append({'title': episode_title, 'status': 0, 'rating_key': episode.ratingKey, 'percentage': 0})
        queue.put({'media': episode,
                   'section': plex_section,
                   'show': show
                   })
        finish_message = episode.title + ' of ' + show.title + ' added to downloads'
    flash(finish_message, category='success')
    return redirect('/section/' + section_id)

@app.route('/settings', methods=['GET', 'POST'])
def settings():
    if request.method == "POST":
        config.set("Main", "PlexUsername", request.form['username'])
        config.set("Main", "PlexPassword", request.form['password'])
        config.set("Transcode", "MaxBitrate", request.form['max-bitrate'])
        config.set("Transcode", "VideoResolution", request.form['video-resolution'])
        if os.path.isdir(request.form['download-path']):
            config.set('Main', 'SavePath', request.form['download-path'])
        config_file = open('config.ini', 'w')
        config.write(config_file)
        config_file.close()
        config.read(os.path.join(os.path.dirname(sys.argv[0]), "config.ini"))
    return render_template('settings.html', username=config.get("Main", "PlexUsername"),
                           password=config.get("Main", "PlexPassword"),
                           video_resolution=config.get('Transcode', 'VideoResolution'),
                           video_bitrate=config.get('Transcode', 'MaxBitrate'),
                           download_path=config.get('Main', 'SavePath'))

@app.route('/queue')
def queue_list_page():
    return render_template('queue.html', queue=enumerate(queue_list))


@app.context_processor
def inject_plex_sections():
    return dict(plex_sections=plex_sections)


@sio.on('connect', namespace="/")
def connect():
    #sio.emit("notification", {"body": "Connected"})
    #print('connect ')
    pass

@sio.on('disconnect', namespace="/")
def disconnect():
    #print('disconnect ')
    pass

if __name__ == "__main__":
    thread = ThreadDownload(queue)
    thread.setDaemon(True)
    thread.start()
    app.debug = False
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    sio.run(app)
