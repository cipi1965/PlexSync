# -*- coding: utf-8 -*-
import imp

def array_split(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr = arr[size:]
    arrs.append(arr)
    return arrs

def module_exists(module):
    try:
        imp.find_module(module)
        return True
    except ImportError:
        return False

def config_array_split(config):
    return [e.strip() for e in config.split(',')]

def search_in_dictionary_array(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1

def human_size(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def percentage(total, partial):
    if int(partial / (total / 100)) > 100:
        return 100
    else:
        return int(partial/(total/100))